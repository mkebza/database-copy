<?php

namespace AppBundle;

use AppBundle\DependencyInjection\Compiler\LoaderPass;
use AppBundle\DependencyInjection\Compiler\RemotePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function build(ContainerBuilder $container) {
        parent::build($container);
        $container->addCompilerPass(new RemotePass());
        $container->addCompilerPass(new LoaderPass());
    }
}
