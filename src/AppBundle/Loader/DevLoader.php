<?php
/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 19:41
 */

namespace AppBundle\Loader;


use Symfony\Component\Process\Process;

class DevLoader implements LoaderInterface {
    protected $user;
    protected $password;
    protected $ssh;

    protected $list = [];

    /**
     * DevLoader constructor.
     * @param $user
     * @param $password
     * @param $ssh
     */
    public function __construct($user, $password, $ssh) {
        $this->user = $user;
        $this->password = $password;
        $this->ssh = $ssh;
    }


    public function load($database, $file) {
        $remoteFileTmp = '/var/www/db_tmp.sql';
        $command = sprintf("scp %s %s:%s", $file, $this->ssh, $remoteFileTmp);
        $process = new Process($command);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new \RuntimeException('Unable to SCP file to dev');
        }


        $command = sprintf("ssh -t %s \"mysql -u%s -p%s %s < %s\"", $this->ssh, $this->user, $this->password, $database, $remoteFileTmp);
        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new \RuntimeException('Unable to import file to MySQL');
        }
    }

    public function getList() {
        if (!empty($this->list)) {
            return $this->list;
        }

        $command = sprintf("ssh -t %s \"mysql -u%s -p%s -N -e 'SHOW DATABASES'\"", $this->ssh, $this->user, $this->password);

        $process = new Process($command);
        $process->setTimeout(900);
        $process->run();
        $lines = explode("\n", $process->getOutput());

        foreach ($lines as $line) {
            if (empty($line)) {
                continue;
            }

            $newLocal = new LoaderObject();
            $newLocal->setName($line);

            $this->list[$newLocal->getName()] = $newLocal;
        }

        if (empty($this->list)) {
            throw new \LogicException('No suitable target');
        }

        return $this->list;
    }


    public function getName() {
        return 'dev';
    }
}