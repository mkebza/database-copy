<?php
/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 19:41
 */

namespace AppBundle\Loader;


use Symfony\Component\Process\Process;

class MacLoader implements LoaderInterface {
    protected $user;
    protected $password;

    protected $list = [];

    /**
     * MacLoader constructor.
     * @param string $user
     * @param string $password
     */
    public function __construct($user, $password) {
        $this->user = $user;
        $this->password = $password;
    }


    public function load($database, $file) {
        $command = sprintf("mysql -u%s -p%s %s < %s", $this->user, $this->password, $database, $file);

        $process = new Process($command);
        $process->setTimeout(900);
        $process->run();
    }

    public function getList() {
        if (!empty($this->list)) {
            return $this->list;
        }

        $command = sprintf("mysql -u%s -p%s -N -e 'SHOW DATABASES'", $this->user, $this->password);

        $process = new Process($command);
        $process->run();
        $lines = explode("\n", $process->getOutput());

        foreach ($lines as $line) {
            if (empty($line)) {
                continue;
            }

            $newLocal = new LoaderObject();
            $newLocal->setName($line);

            $this->list[$newLocal->getName()] = $newLocal;
        }

        if (empty($this->list)) {
            throw new \LogicException('No suitable target');
        }

        return $this->list;
    }


    public function getName() {
        return 'mac';
    }
}