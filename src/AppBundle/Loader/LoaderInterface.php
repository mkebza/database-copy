<?php
/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 19:41
 */

namespace AppBundle\Loader;


interface LoaderInterface {
    public function load($database, $file);
    public function getName();
    public function getList();
}