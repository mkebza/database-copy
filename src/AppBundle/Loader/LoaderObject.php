<?php
/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 28/04/2017
 * Time: 11:45
 */

namespace AppBundle\Loader;


class LoaderObject {
    protected $name;

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return LoaderObject
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    function __toString() {
        return $this->getName();
    }
}