<?php
namespace AppBundle\Remote;

/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 17:45
 */
interface RemoteInterface {
    public function getList();

    /**
     * @param $name
     * @return RemoteObject
     */
    public function getObject($name);
    public function download(RemoteObject $object, $tmpFileName);
    public function getName();
}