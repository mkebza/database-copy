<?php
/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 18:33
 */

namespace AppBundle\Remote;

class RemoteObject {
    protected $key;
    protected $name;
    protected $handler;
    protected $extra;
    protected $size;


    /**
     * @return mixed
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return RemoteObject
     */
    public function setKey($key) {
        $this->key = $key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return RemoteObject
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHandler() {
        return $this->handler;
    }

    /**
     * @param mixed $handler
     * @return RemoteObject
     */
    public function setHandler($handler) {
        $this->handler = $handler;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtra() {
        return $this->extra;
    }

    /**
     * @param mixed $extra
     * @return RemoteObject
     */
    public function setExtra($extra) {
        $this->extra = $extra;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize() {
        return $this->size;
    }

    /**
     * @param mixed $size
     * @return RemoteObject
     */
    public function setSize($size) {
        $this->size = $size;
        return $this;
    }

    function __toString() {
        return $this->getName();
    }


}