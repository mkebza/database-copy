<?php
namespace AppBundle\Remote;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 17:45
 */
class DonutRemote implements RemoteInterface {
    protected $list = [];
    protected $ssh;

    /**
     * HomerRemote constructor.
     * @param $ssh
     */
    public function __construct($ssh) {
        $this->ssh = $ssh;
    }


    public function getName() {
        return 'donut';
    }

    public function getObject($name) {
        $this->getList();

        return $this->list[$name];
    }


    public function getList() {
        if (!empty($this->list)) {
            return $this->list;
        }
        $command = sprintf('ssh -t %s "find \$(ls -td /mnt/backup/db/*/ | head -2 | tail -1) -name \"*.gz\" -ls; bash;"', $this->ssh);
        $process = new Process($command);
        $process->run();
        $output = $process->getOutput();

        if (empty($output)) {
            throw new \LogicException("Can't load information from remote host");
        }

        foreach (explode("\n", $output) as $line) {
            if (empty($line)) {
                continue;
            }
            $newRemote = new RemoteObject();

            $values = preg_split("@\s+@", $line);
            $newRemote
                ->setName(substr(basename($values[11]), 0, -15))
                ->setKey($values[11])
                ->setSize($values[7])
                ->setHandler(self::class);

            $this->list[$newRemote->getName()] = $newRemote;
        }

        if (empty($this->list)) {
            throw new \LogicException("No applicable file on server");
        }

        return $this->list;
    }

    public function download(RemoteObject $object, $tmpFileName) {
        $builder = new ProcessBuilder([]);

        $tmpFileNameGzip = $tmpFileName.'.gz';

        $builder
            ->setPrefix('scp')
            ->add(sprintf('%s:%s', $this->ssh, $object->getKey()))
            ->add($tmpFileNameGzip);

        $process = $builder->getProcess();
        $process->setTimeout(900);
        $process->run();

        $process = new Process(sprintf('gunzip -c %s > %s', $tmpFileNameGzip, $tmpFileName));
        $process->run();

        unlink($tmpFileNameGzip);
    }
}