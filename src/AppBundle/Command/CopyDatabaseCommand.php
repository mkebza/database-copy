<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 17:47
 */
class CopyDatabaseCommand extends ContainerAwareCommand {
    protected $remote;
    protected $remoteObjectName;
    protected $loader;
    protected $target;

    protected function configure() {
        $this
            ->setName('app:update')
            ->addOption('remote', 'r', InputArgument::OPTIONAL, 'Remote handler')
            ->addOption('source', 's', InputArgument::OPTIONAL, 'Source database file name')
            ->addOption('loader', 'l', InputArgument::OPTIONAL, 'Loader handler')
            ->addOption('target', 't', InputArgument::OPTIONAL, 'Target database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $stopwatch = $this->getContainer()->get('debug.stopwatch');
        try {
            $this->interactRemote($input, $output);
            $this->interactSource($input, $output);
            $this->interactLoader($input, $output);
            $this->interactTarget($input, $output);


            $stopwatch->start('total');
            $copyManager = $this->getContainer()->get('copy_manager');

            $output->writeln('Initializing...');
            $remoteHandler = $copyManager->getRemoteHandler($this->remote);
            $remoteObject = $remoteHandler->getObject($this->remoteObjectName);

            $tmpFileName = sprintf('%s/../var/database_%s.sql',
                $this->getContainer()->getParameter('kernel.root_dir'),
                basename($remoteObject->getName())
            );

            $output->writeln(sprintf('Downloading file %s, size %s ...',
                $remoteObject->getName(),
                $this->humanFilesize($remoteObject->getSize())
            ));

            $remoteHandler->download($remoteObject, $tmpFileName);

            $output->writeln(sprintf('Importing %s, size %s ...', basename($tmpFileName), $this->humanFilesize(filesize($tmpFileName))));
            $copyManager->getloaderHandler($this->loader)->load($this->target, $tmpFileName);

            $output->writeln('Cleaning up ...');
            unset($tmpFileName);

            $event = $stopwatch->stop('total');
            $output->writeln(sprintf('<fg=green>Done!!!</> Total time: %s', $event->getDuration()/1000));
        } catch (\Exception $e) {
            $output->writeln(sprintf("<error>Error occured: %s</error>", $e->getMessage()));
        }

    }


    protected function humanFilesize($bytes, $dec = 2) {
        $size   = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$dec}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    protected function interactRemote(InputInterface $input, OutputInterface $output) {
        if ($input->getOption('remote') == null) {
            $helper = $this->getHelper('question');
            $question = new ChoiceQuestion(
                'Which server?',
                $this->getContainer()->get('copy_manager')->getRemoteNames()
            );
            $question->setErrorMessage('Color %s is invalid.');

            $this->remote = $helper->ask($input, $output, $question);
        } else {
            $this->remote = $input->getOption('remote');
        }
    }

    protected function interactLoader(InputInterface $input, OutputInterface $output) {
        if ($input->getOption('loader') == null) {
            $helper = $this->getHelper('question');
            $question = new ChoiceQuestion(
                'Which loader?',
                $this->getContainer()->get('copy_manager')->getLoaderNames()
            );
            $question->setErrorMessage('Color %s is invalid.');

            $this->loader = $helper->ask($input, $output, $question);
        } else {
            $this->loader = $input->getOption('loader');
        }
    }

    protected function interactTarget(InputInterface $input, OutputInterface $output) {
        if ($input->getOption('target') == null) {
            $helper = $this->getHelper('question');
            $question = new ChoiceQuestion(
                'Which loader?',
                $this->getContainer()->get('copy_manager')->getloaderHandler($this->loader)->getList()
            );
            $question->setErrorMessage('Color %s is invalid.');

            $this->target = $helper->ask($input, $output, $question);
        } else {
            $this->target = $input->getOption('target');
        }
    }

    protected function interactSource(InputInterface $input, OutputInterface $output) {
        if ($input->getOption('source') == null) {
            $helper = $this->getHelper('question');
            $question = new ChoiceQuestion(
                'Which file?',
                $this->getContainer()->get('copy_manager')->getRemoteHandler($this->remote)->getList()
            );
            $question->setErrorMessage('Color %s is invalid.');

            $this->remoteObjectName = $helper->ask($input, $output, $question);
        } else {
            $this->remoteObjectName = $input->getOption('source');
        }
    }
}