<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class LoaderPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('copy_manager')) {
            return;
        }

        $definition = $container->findDefinition('copy_manager');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('copy.loader');

        foreach ($taggedServices as $id => $tags) {
            // add the transport service to the ChainTransport service
            $definition->addMethodCall('addLoader', array(new Reference($id)));
        }
    }
}
