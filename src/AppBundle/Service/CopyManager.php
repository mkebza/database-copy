<?php
/**
 * User: Marek Kebza <marek@kebza.cz>
 * Date: 27/04/2017
 * Time: 18:02
 */

namespace AppBundle\Service;


use AppBundle\Loader\LoaderInterface;
use AppBundle\Remote\RemoteInterface;
use AppBundle\Remote\RemoteObject;

class CopyManager {
    protected $remotes = [];
    protected $loaders = [];
    protected $tmpDir;

    /**
     * CopyManager constructor.
     * @param $tmpDir
     */
    public function __construct($tmpDir) {
        $this->tmpDir = $tmpDir;
    }


    public function addRemote(RemoteInterface $remote) {
        $this->remotes[$remote->getName()] = $remote;
    }

    public function addLoader(LoaderInterface $loader) {
        $this->loaders[$loader->getName()] = $loader;
    }

    /**
     * @param $loader
     * @return LoaderInterface
     */
    public function getLoaderHandler($loader) {
        if (!array_key_exists($loader, $this->loaders)) {
            throw new \InvalidArgumentException(sprintf("Non existent loader %s", $loader));
        }
        return $this->loaders[$loader];
    }

    public function getRemoteNames() {
        return array_combine(array_keys($this->remotes), array_keys($this->remotes));
    }

    public function getLoaderNames() {
        return array_combine(array_keys($this->loaders), array_keys($this->loaders));
    }


    public function copy($remoteName, RemoteObject $object, $loaderName, $localTarget = null) {
        try {
            $remoteHandler = $this->getRemoteHandler($remoteName);

            $tmpFileName = sprintf('%s/database_%s.sql', $this->tmpDir, basename($object->getName()));
            $remoteHandler->download($object, $tmpFileName);

            $this->getloaderHandler($loaderName)->load($localTarget, $tmpFileName);
            unset($tmpFileName);
        } catch (\Exception $e) {
            throw new $e;
        }
    }

    /**
     * @param $name
     * @return RemoteInterface
     */
    public function getRemoteHandler($name) {
        if (!array_key_exists($name, $this->remotes)) {
            throw new \InvalidArgumentException(sprintf("Invalid remote handler %s", $name));
        }
        return $this->remotes[$name];
    }
}